import React, {Fragment} from 'react';
import Spinner from "../Spinner/Spinner";

import './Backdrop.css';


const Backdrop = () => (
    <Fragment>
        <div className="Backdrop"/>
        <Spinner/>
    </Fragment>
);

export default Backdrop;
