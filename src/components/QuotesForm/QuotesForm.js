import React, {Component} from 'react';
import {Button, Form, FormGroup, Input, Label} from "reactstrap";
import {QUOTES_CATEGORIES} from "../../constants";

import './QuotesForm.css';

class QuotesForm extends Component {
    constructor(props) {
        super (props);
        if (this.props.quotes) {
            this.state = {...props.quotes}
        } else {
            this.state = {
                author: '',
                category: Object.keys(QUOTES_CATEGORIES)[0],
                text: '',
            };
        }
    }

    valueChanged = event => {
        const {name, value} = event.target;
        this.setState({[name]: value});
    };

    saveQuoteHandler = event => {
        event.preventDefault();
        this.props.onSubmit({...this.state});
    };

    render() {
        return (
            <Form className="form" onSubmit={this.saveQuoteHandler}>
                <FormGroup row>
                    <Label for="category">Category</Label>
                    <Input type="select"
                           name="category" id="category"
                           placeholder="Select category"
                           value={this.state.category}
                           onChange={this.valueChanged}
                    >
                        {Object.keys(QUOTES_CATEGORIES).map(quotesId => (
                            <option key={quotesId} value={quotesId}>{QUOTES_CATEGORIES[quotesId]}</option>
                        ))}
                    </Input>
                </FormGroup>
                <FormGroup row>
                    <Label for="author">Author</Label>
                    <Input type="text" name="author" id="author" placeholder="Enter Name of Author"
                        value={this.state.author} onChange={this.valueChanged}
                    />
                </FormGroup>
                <FormGroup row>
                    <Label for="text">Quote text</Label>
                    <Input type="textarea" name="text" id="text" placeholder="Enter quote text"
                           value={this.state.text} onChange={this.valueChanged}
                    />
                </FormGroup>
                <FormGroup>
                    <Button color="success" size="lg" type="submit">Save</Button>
                </FormGroup>
            </Form>
        );
    }
}

export default QuotesForm;