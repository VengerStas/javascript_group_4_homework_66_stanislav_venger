import React, {Component, Fragment} from 'react';
// import Spinner from "../components/UI/Spinner/Spinner";
import Backdrop from "../components/UI/Backdrop/Backdrop";
const WithLoader = (WrappedComponent, axios) => {
    return class WithLoaderHOC extends Component {
        constructor(props) {
            super (props);
            this.state = {
                loading: false
            };

            this.state.interceptorIdReq = axios.interceptors.request.use(req => {
                this.setState({loading: true});
                return req
            }, error =>  {
                this.setState({error});
                throw error;
            });

            this.state.interceptorIdRes = axios.interceptors.response.use(res => {
                this.setState({loading: false});
                return res
            }, error =>  {
                this.setState({error});
                throw error;
            });
        }

        componentWillUnmount() {
            axios.interceptors.request.eject(this.state.interceptorIdReq);
            axios.interceptors.response.eject(this.state.interceptorIdRes);
        }

        render () {
            return (
                <Fragment>
                    {this.state.loading ? <Backdrop/> : null}
                    <WrappedComponent {...this.props}/>
                </Fragment>
            )
        }
    }
};


export default WithLoader;
