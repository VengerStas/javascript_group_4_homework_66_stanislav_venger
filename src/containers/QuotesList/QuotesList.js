import React, {Component} from 'react';
import axios from '../../axios-quotes';
import withLoader from '../../hoc/withLoader';
import {Button, Card, CardBody, CardText, Col, Nav, NavItem, NavLink, Row} from "reactstrap";
import {QUOTES_CATEGORIES} from "../../constants";
import {NavLink as RouterNavLink} from "react-router-dom";

import './QuotesList.css';

class QuotesList extends Component {
    state = {
        quotes: null,
    };

    loadQuote () {
        let url = 'quotes.json';

        const quoteId = this.props.match.params.quoteId;
        if (quoteId) {
            url += `?orderBy="category"&equalTo="${quoteId}"`;
        }
        axios.get(url).then(response => {
            if (response.data === null) {
                const quotes = [];
                this.setState({quotes});
                if (!response.data) return;
            }
            const quotes = Object.keys(response.data).map(id => {
                return {...response.data[id], id}
            });

            this.setState({quotes});
        })
    }

    componentDidMount () {
        this.loadQuote();
    }

    componentDidUpdate (prevProps) {
        if (this.props.match.params.quoteId !== prevProps.match.params.quoteId) {
            this.loadQuote();
        }
    }

    deleteHandler = quoteId => {
        axios.delete('quotes/' + quoteId + '.json').then(() => {
            this.props.history.replace('/');
            this.loadQuote();
        });

    };

    render() {
        let quotes = null;

        if (this.state.quotes) {
            quotes = this.state.quotes.map(quote => (
                <Card key={quote.id}>
                    <CardBody>
                        <CardText>"{quote.text}"</CardText>
                        <CardText>- {quote.author}</CardText>
                        <Button tag={RouterNavLink} to={"/quotes/" + quote.id + "/edit"}>Edit</Button>
                        <Button onClick={() => this.deleteHandler(quote.id)}>Delete</Button>
                    </CardBody>
                </Card>
            ))
        }
        return (
           <Row>
               <Col sm={4}>
                   <Nav vertical>
                       <NavItem>
                           <NavLink tag={RouterNavLink} to="/">All</NavLink>
                       </NavItem>
                       {Object.keys(QUOTES_CATEGORIES).map(quoteId => (
                           <NavItem key={quoteId}>
                               <NavLink
                                    tag={RouterNavLink}
                                    to={"/quotes/" + quoteId}
                                    exact
                               >
                                   {QUOTES_CATEGORIES[quoteId]}
                               </NavLink>
                           </NavItem>
                       ))}
                   </Nav>
               </Col>
               <Col sm={8}>
                   {quotes}
               </Col>
           </Row>
        );
    }
}

export default withLoader(QuotesList, axios);
